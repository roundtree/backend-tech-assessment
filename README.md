# Backend Technical Test
## Assignment details
### Overview
The goal of the technical test is to create a Java web application based on the standard servlet spec.
The application should provide a RESTful API that retrieve transactions from OpenBank sandbox
and transforms the data using a provided mapping (see details below).
### Requirements
The application should expose three endpoints:
*  Transactions list
* Transaction filter based on transaction type
* Total amount for transaction type
### Development information
* [Transaction list service](https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions) and [transaction list API](https://github.com/OpenBankProject/OBP-API/wiki/REST-API-V1.2.1#transactions)
* OpenBank <-> our application mapping

| Our  application field | Open bank field |
| ------ | ------ |
| id | id |
| accountId | this_account.id |
| counterpartyAccount | other_account.number |
| counterpartyName | other_account.holder.name |
| counterPartyLogoPath | other_account.metadata.image_URL |
| instructedAmount | details.value.amount |
| instructedCurrency | details.value.currency |
| transactionAmount | details.value.amount |
| transactionCurrency | details.value.currency |
| transactionType | details.type |
| description | details.description |

### Constraints
* __Usage of Spring boot is forbidden because we want you to implement the web descriptor
configurations.__
### Mandatory features
* The test should be built using Java, Spring and Maven. If you do not have experience in
these frameworks, please feel free to use any other alternative.
* Use Spring security to secure the application between anonymous and logged in users
* All code and classes delivered should have Junit tests with 70% code coverage

### Bonus features
* Use any embedded web servlet container to startup the web application in maven
* Documentation of classes/code/packaging (swagger or RAML or any other tools)
* Use Camel to route the web service call
* Configure logging
### Mandatory Packaging
* The project should be delivered as a Web Application
* The project should be build using maven
* Short document explaining how to build and start the project
### Platform support
* Jetty 9 or Tomcat 7 or 8
* JDK 7 or 8
### Suggestion
Keep it simple. Part of the evaluation consists on being able to follow the requirements, and not over
engineer the solution.

## How to run
From project root:

    mvn clean package
    java -jar target/backend-tech-test-1.0-SNAPSHOT-jar-with-dependencies.jar
    
### Example URLs
Note: all URLs below require basic authentication (username: admin, password: admin)
#### Fetch transactions:
http://localhost:8080/api/transactions/{bankId}/{accountId}?type={optionalType}
http://localhost:8080/api/transactions/rbs/savings-kids-john
http://localhost:8080/api/transactions/rbs/savings-kids-john?type=SANDBOX_TAN

#### Total amount for transaction type
http://localhost:8080/api/transactions/{bankId}/{accountId}/total?type={mandatoryType}
http://localhost:8080/api/transactions/rbs/savings-kids-john/total?type=SANDBOX_TAN
