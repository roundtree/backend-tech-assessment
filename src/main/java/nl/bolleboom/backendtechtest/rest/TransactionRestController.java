package nl.bolleboom.backendtechtest.rest;

import nl.bolleboom.backendtechtest.dto.TotalAmount;
import nl.bolleboom.backendtechtest.dto.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/transactions")
public class TransactionRestController {

    private final RestTemplate restTemplate;

    @Autowired
    public TransactionRestController(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/{bankId}/{accountId}")
    public List<Transaction> transactions(@PathVariable("bankId") final String bankId,
                                          @PathVariable("accountId") final String accountId,
                                          @RequestParam(value = "type", required = false) final String type) {
        final String url = String.format("https://apisandbox.openbankproject.com/obp/v1.2.1/banks/%s/accounts/" +
                "%s/public/transactions", bankId, accountId);

        return restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Transaction>>() {
        }).getBody()
                .stream()
                .filter(transaction -> type == null || transaction.hasType(type))
                .collect(Collectors.toList());
    }

    @GetMapping("/{bankId}/{accountId}/total")
    public TotalAmount totalAmount(@PathVariable("bankId") final String bankId,
                                   @PathVariable("accountId") final String accountId,
                                   @RequestParam("type") final String type) {
        final List<Transaction> transactions = transactions(bankId, accountId, type);

        return transactions
                .stream()
                .map(Transaction::getTransactionAmount)
                .reduce(BigDecimal::add)
                .map(total -> new TotalAmount(total, type))
                .orElse(new TotalAmount(BigDecimal.ZERO, type));
    }
}