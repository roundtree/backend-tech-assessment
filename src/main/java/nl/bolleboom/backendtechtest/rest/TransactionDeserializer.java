package nl.bolleboom.backendtechtest.rest;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import nl.bolleboom.backendtechtest.dto.Transaction;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TransactionDeserializer extends JsonDeserializer<List<Transaction>> {

    @Override
    public List<Transaction> deserialize(JsonParser p, DeserializationContext ctxt) {
        final List<Transaction> transactions = new ArrayList<>();
        final ObjectCodec codec = p.getCodec();

        JsonNode rootNode;

        try {
            rootNode = codec.readTree(p);
        } catch (IOException e) {
            e.printStackTrace();
            return transactions;
        }

        final ArrayNode transactionsArrayNode = (ArrayNode) rootNode.get("transactions");
        transactionsArrayNode.forEach(transactionNode -> {
            final Transaction transaction = new Transaction();

            transaction.setId(transactionNode.get("id").asText());
            transaction.setAccountId(transactionNode.get("this_account").get("id").asText());

            final JsonNode otherAccountNode = transactionNode.get("other_account");
            transaction.setCounterpartyAccount(otherAccountNode.get("number").asText());
            transaction.setCounterpartyName(otherAccountNode.get("holder").get("name").asText());
            transaction.setCounterPartyLogoPath(otherAccountNode.get("metadata").get("image_URL").asText(null));

            final JsonNode detailsNode = transactionNode.get("details");
            transaction.setAmount(new BigDecimal(detailsNode.get("value").get("amount").asText()));
            transaction.setCurrency(detailsNode.get("value").get("currency").asText());
            transaction.setTransactionType(detailsNode.get("type").asText());
            transaction.setDescription(detailsNode.get("description").asText(null));

            transactions.add(transaction);
        });

        return transactions;
    }
}