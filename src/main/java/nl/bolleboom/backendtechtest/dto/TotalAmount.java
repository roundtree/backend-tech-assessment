package nl.bolleboom.backendtechtest.dto;

import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

public class TotalAmount {

    private final  BigDecimal totalAmount;
    private final String transactionType;


    public TotalAmount(final BigDecimal totalAmount, final String transactionType) {
        this.totalAmount = requireNonNull(totalAmount);
        this.transactionType = requireNonNull(transactionType);
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }
}
