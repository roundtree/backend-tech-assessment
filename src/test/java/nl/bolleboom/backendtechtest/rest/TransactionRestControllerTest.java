package nl.bolleboom.backendtechtest.rest;

import nl.bolleboom.backendtechtest.dto.TotalAmount;
import nl.bolleboom.backendtechtest.dto.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransactionRestControllerTest {

    private RestTemplate restTemplate;
    private TransactionRestController transactionRestController;

    @Before
    public void setup() {
        restTemplate = mock(RestTemplate.class);
        transactionRestController = new TransactionRestController(restTemplate);
    }

    @Test
    public void testGetTransactions() {
        final List<Transaction> transactionResponse = Arrays.asList(new Transaction(), new Transaction());
        final ResponseEntity<List<Transaction>> response = new ResponseEntity<>(transactionResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), eq(null), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        final List<Transaction> transactions1 = transactionRestController.transactions("bankId", "accountId", null);
        assertThat(transactions1).hasSize(2);
    }

    @Test
    public void testGetTransactionsWithFilter() {
        final Transaction transaction1 = new Transaction();
        transaction1.setTransactionType("type1");

        final Transaction transaction2 = new Transaction();
        transaction2.setTransactionType("type2");

        final ResponseEntity<List<Transaction>> response = new ResponseEntity<>(Arrays.asList(transaction1, transaction2), HttpStatus.OK);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), eq(null), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        final List<Transaction> transactionsType1 = transactionRestController.transactions("bankId", "accountId", "type1");
        assertThat(transactionsType1).hasSize(1);
        assertThat(transactionsType1.get(0).getTransactionType()).isEqualTo("type1");

        final List<Transaction> transactionsType2 = transactionRestController.transactions("bankId", "accountId", "type2");
        assertThat(transactionsType2).hasSize(1);
        assertThat(transactionsType2.get(0).getTransactionType()).isEqualTo("type2");
    }

    @Test
    public void testGetTransactionsTotalAmount() {
        final Transaction transaction1 = new Transaction();
        transaction1.setTransactionType("type1");
        transaction1.setAmount(new BigDecimal("90.00"));

        final Transaction transaction2 = new Transaction();
        transaction2.setTransactionType("type1");
        transaction2.setAmount(new BigDecimal("-100.00"));

        final ResponseEntity<List<Transaction>> response = new ResponseEntity<>(Arrays.asList(transaction1, transaction2), HttpStatus.OK);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), eq(null), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        final TotalAmount transactionAmount = transactionRestController.totalAmount("bankId", "accountId", "type1");
        assertThat(transactionAmount.getTotalAmount()).isEqualTo("-10.00");
        assertThat(transactionAmount.getTransactionType()).isEqualTo("type1");
    }

    @Test
    public void testGetTransactionsTotalAmountNoTransactions() {
        final ResponseEntity<List<Transaction>> response = new ResponseEntity<>(Collections.emptyList(), HttpStatus.OK);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), eq(null), any(ParameterizedTypeReference.class)))
                .thenReturn(response);

        final TotalAmount transactionAmount = transactionRestController.totalAmount("bankId", "accountId", "type1");
        assertThat(transactionAmount.getTotalAmount()).isZero();
        assertThat(transactionAmount.getTransactionType()).isEqualTo("type1");
    }
}