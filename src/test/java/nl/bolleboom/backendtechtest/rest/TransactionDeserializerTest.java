package nl.bolleboom.backendtechtest.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import nl.bolleboom.backendtechtest.dto.Transaction;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionDeserializerTest {

    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(List.class, new TransactionDeserializer());
        objectMapper.registerModule(module);
    }

    @Test
    public void testDeserializeTransactions() throws IOException {
        final List<Transaction> transactions = objectMapper.readValue(new File("src/test/resources/transactions.json"), List.class);
        assertThat(transactions).hasSize(2);

        final Transaction transaction = transactions.get(0);
        assertThat(transaction.getId()).isEqualTo("897706c1-dcc6-4e70-9d85-8a537c7cbf3e");
        assertThat(transaction.getAccountId()).isEqualTo("savings-kids-john");
        assertThat(transaction.getCounterpartyAccount()).isEqualTo("savings-kids-john");
        assertThat(transaction.getCounterpartyName()).isEqualTo("ALIAS_49532E");
        assertThat(transaction.getCounterPartyLogoPath()).isNull();
        assertThat(transaction.getInstructedAmount()).isEqualTo("-90.00");
        assertThat(transaction.getInstructedCurrency()).isEqualTo("GBP");
        assertThat(transaction.getTransactionAmount()).isEqualTo("-90.00");
        assertThat(transaction.getTransactionCurrency()).isEqualTo("GBP");
        assertThat(transaction.getTransactionType()).isEqualTo("SANDBOX_TAN");
        assertThat(transaction.getDescription()).isEqualTo("Gift");
    }

    @Test
    public void testDeserializeEmptyResponse() throws IOException {
        final List<Transaction> transactions = objectMapper.readValue("{ \"transactions\": [] }", List.class);
        assertThat(transactions).isEmpty();
    }
}